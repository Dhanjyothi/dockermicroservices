package com.hcl.webinar.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.hcl.webinar.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer,Integer>,JpaRepository<Customer,Integer> {

	
	
}
