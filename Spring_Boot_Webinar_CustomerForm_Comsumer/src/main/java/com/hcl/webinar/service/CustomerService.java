package com.hcl.webinar.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import com.hcl.webinar.model.Customer;
import com.netflix.appinfo.InstanceInfo;

import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@Service
public class CustomerService {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private DiscoveryClient discoveryClient;

	public Collection<Customer> getAllCustomers() {
		Collection<Customer> list=restTemplate.getForObject("http://customer-producer/getAllCustomers", Collection.class);
		return list;
		/*List<ServiceInstance> instances = discoveryClient.getInstances("customer-producer");
		ServiceInstance serviceInstance = instances.get(0);
		String baseUrl = serviceInstance.getUri().toString() + "getAllCustomers";
		Collection<Customer> list = restTemplate.getForObject(baseUrl, Collection.class);
		return list;*/
	}

	public String addCustomer(Customer customer) {
		
		/*List<ServiceInstance> instances = discoveryClient.getInstances("customer-producer");
		ServiceInstance serviceInstance = instances.get(0);
		String baseUrl = serviceInstance.getUri().toString() + "saveCustomer";*/
		String result = restTemplate.postForObject("http://customer-producer/saveCustomer", customer, String.class);
		return result;

	}

}
